﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour
{
    public List<GameObject> startPositions, spawnPositions, levelCollisions, playerStart = new List<GameObject>();
    public int randomLevel;
    public InstructionHandler iScript;
    
    // Start is called before the first frame update
    void Start()
    {
        RandomLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RandomLevel()
    {
        randomLevel = Random.Range(0,4);

        foreach(GameObject x in startPositions)
        {
            x.SetActive(false);
        }
        startPositions[randomLevel].SetActive(true);

        foreach(GameObject x in spawnPositions)
        {
            x.SetActive(false);
        }
        spawnPositions[randomLevel].SetActive(true);

        foreach(GameObject x in levelCollisions)
        {
            x.SetActive(false);
        }
        levelCollisions[randomLevel].SetActive(true);

        foreach (GameObject x in playerStart)
        {
            x.SetActive(false);
        }
        playerStart[randomLevel].SetActive(true);

        iScript.ReturnHome();
    }
}
