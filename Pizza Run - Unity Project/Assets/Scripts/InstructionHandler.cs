﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InstructionHandler : MonoBehaviour
{
    public List<GameObject> instList = new List<GameObject>();
    public List<Sprite> instDispList = new List<Sprite>();
    public List<string> instString = new List<string>();
    public int nextInstSlot;
    public bool canEdit;
    public GameObject player;
    private PlayerMovement playerMovement;
    public LevelScript lScript;

    private Color32 black = new Color32(0,0,0,255);
    private Color32 white = new Color32(255,255,255,255);   

    // Start is called before the first frame update
    void Start()
    {
        canEdit = true;
        playerMovement = player.GetComponent<PlayerMovement>();
        ResetInstructions();
        ReturnHome();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetInstructions()
    {
        if(canEdit)
        {
            foreach(GameObject i in instList)
            {
                i.transform.parent.gameObject.GetComponent<Image>().color = white;
                i.GetComponent<Image>().color = white;
                i.GetComponent<Image>().sprite = null;
            }
            
            nextInstSlot = 0;
            instString = new List<string>();
            playerMovement.currentInstruction = 0;
        }
    }

    public void AddInstruction(string instDir)
    {
        if(canEdit && nextInstSlot <= 29)
        {
            switch(instDir)
            {
                case "F":
                    instList[nextInstSlot].GetComponent<Image>().sprite = instDispList[0];
                break;

                case "B":
                    instList[nextInstSlot].GetComponent<Image>().sprite = instDispList[1];
                break;

                case "R":
                    instList[nextInstSlot].GetComponent<Image>().sprite = instDispList[2];
                break;

                case "L":
                    instList[nextInstSlot].GetComponent<Image>().sprite = instDispList[3];
                break;
            }
            instString.Add(instDir);
            instList[nextInstSlot].GetComponent<Image>().color = black;
            nextInstSlot++;
        }
    }

    public void RemoveInstruction()
    {
        if(canEdit && nextInstSlot > 0)
        {
            nextInstSlot --;
            instString.RemoveAt(nextInstSlot);
            instList[nextInstSlot].GetComponent<Image>().sprite = null;
            instList[nextInstSlot].GetComponent<Image>().color = white;
        }
    }

    public void ReturnHome()
    {
        if(canEdit)
        {
            playerMovement.movePoint.position = lScript.playerStart[lScript.randomLevel].transform.position;
            player.transform.GetChild(0).transform.rotation = Quaternion.identity;
            playerMovement.playerRotation = 0;
            player.transform.position = lScript.playerStart[lScript.randomLevel].transform.position;
            playerMovement.currentInstruction = 0;
        }
    }
}
