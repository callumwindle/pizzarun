﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float moveModify;
    public int playerRotation, totalInstructions, currentInstruction, remainingInstructions;
    public Vector3 instructedDirection;
    public Transform movePoint, rayStart, rayEnd;
    public GameObject movePointContainer;
    public LayerMask moveStoppers;
    public InstructionHandler instructionHandler;
    public RaycastHit2D hit;
    
    private bool isMoving;


    // Start is called before the first frame update
    void Start()
    {
        isMoving = false;
        currentInstruction = 0;
        movePoint.parent = movePointContainer.transform;
        movePoint.position = new Vector3(0f,0f,0f);
    }

    void Update()
    {          
        if(isMoving)
        {
            if(Vector3.Distance(transform.position, movePoint.position) >= 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);
            }
            else
            {
                isMoving = false;
            }
        }
    }
    
    public void ReadInstructions()
    {
        totalInstructions = instructionHandler.instString.Count;
        remainingInstructions = totalInstructions - currentInstruction;
        
        if(remainingInstructions > 0)
        {
            instructionHandler.canEdit = false;
            switch(instructionHandler.instString[currentInstruction])
            {
                case "F":
                    rayEnd.localPosition = new Vector3(0f,1.5f,0f);
                    switch(playerRotation)
                    {
                        case 0:
                            instructedDirection = new Vector3(0f,1f,0f);
                        break;

                        case 1:
                            instructedDirection = new Vector3(1f,0f,0f);
                        break;

                        case 2:
                            instructedDirection = new Vector3(0f,-1f,0f);
                        break;

                        case 3:
                            instructedDirection = new Vector3(-1f,0f,0f);
                        break;
                    }

                    Invoke("ActInstruction", 0.5f);
                break;

                case "B": 
                    rayEnd.localPosition = new Vector3(0f,-1.5f,0f);
                    switch(playerRotation)
                    {
                        case 0:
                            instructedDirection = new Vector3(0f,-1f,0f);
                        break;

                        case 1:
                            instructedDirection = new Vector3(-1f,0f,0f);
                        break;

                        case 2:
                            instructedDirection = new Vector3(0f,1f,0f);
                        break;

                        case 3:
                            instructedDirection = new Vector3(1f,0f,0f);
                        break;
                    }

                    Invoke("ActInstruction", 0.5f);
                break;

                case "R":
                    transform.GetChild(0).transform.Rotate(0f,0f,-90f);
                    if(playerRotation < 3)
                    {
                        playerRotation ++;
                    }
                    else
                    {
                        playerRotation = 0;
                    }

                    currentInstruction ++;
                    Invoke("ReadInstructions", 0.5f);
                break;

                case "L":
                    transform.GetChild(0).transform.Rotate(0f,0f,90f);
                    if(playerRotation > 0)
                    {
                        playerRotation --;
                    }
                    else
                    {
                        playerRotation = 3;
                    }

                    currentInstruction ++;
                    Invoke("ReadInstructions", 0.5f);
                break;
            }
        }
        else
        {
            instructionHandler.canEdit = true;
        } 
        
        
    }

    public void ActInstruction()
    {
        RaycastHit2D hit = Physics2D.Linecast(rayStart.position, rayEnd.position, moveStoppers);
        if(hit.collider == null)
        {
            movePoint.position += instructedDirection;
            isMoving = true;
        }
        currentInstruction ++;
        Invoke("ReadInstructions", 0.5f);
    }

}
