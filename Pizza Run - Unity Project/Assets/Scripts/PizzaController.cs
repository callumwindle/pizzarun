﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PizzaController : MonoBehaviour
{
    public Vector3 pizzaPosition, lastPizzaPosition;
    private Vector3 playerStartPos = new Vector3(-4.5f,-1.5f,0f);
    private Transform spawnPoint;
    public GameObject levelPizzarias, smolGem;
    public int currentScore, currentLevel, possSpawnPositions, randomGemVal, gemValue, collectedGem;
    public Text scoreText;
    public InstructionHandler instructionHandler;
    public UIManager uiMan;
    public List<GameObject> levelSpawnGroups = new List<GameObject>();
    public bool onDelivery;
    public Sprite delSprite, mapSprite;
    public LevelScript lScript;

    // Start is called before the first frame update
    void Start()
    {
        pizzaPosition = new Vector3(10f,10f,10f);
        currentScore = 0;
        EditScore(0);
        currentLevel = 0;
        Invoke("NewOrder", 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewOrder()
    {
        currentLevel = lScript.randomLevel;
        gameObject.GetComponent<SpriteRenderer>().sprite = delSprite;
        transform.localScale = new Vector3(0.25f,0.25f,1f);
        transform.localRotation = Quaternion.identity;
        possSpawnPositions = levelSpawnGroups[currentLevel].gameObject.transform.childCount; 
        spawnPoint = levelSpawnGroups[currentLevel].gameObject.transform.GetChild(Random.Range(0,possSpawnPositions));
        
        if(spawnPoint.position != lastPizzaPosition)
        {
            transform.position = spawnPoint.position;
            onDelivery = true;
            smolGem.SetActive(false);
            instructionHandler.canEdit = true;
            instructionHandler.ResetInstructions();
        }
        else
        {
            NewOrder();
        }
                
    }

    public void NewDelivery()
    {
        smolGem.SetActive(true);
        gameObject.GetComponent<SpriteRenderer>().sprite = mapSprite;
        transform.localScale = new Vector3(0.15f,0.15f,1f);
        transform.localRotation = Quaternion.Euler(0,0,45);
        transform.position = (levelPizzarias.transform.GetChild(currentLevel).gameObject.transform.position) - new Vector3(-0.1f,0.1f,0f);
        onDelivery = false;
        instructionHandler.canEdit = true;
        instructionHandler.ResetInstructions();
        
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(!onDelivery)
        {
            EditScore(10);
            if(collectedGem == 10)
            {
                uiMan.EndScreen();
            }
            else
            {
                collectedGem++;
                NewOrder();
            }
            
        }
        else
        {
            lastPizzaPosition = transform.position;
            NewDelivery();
        }

    }

    private void EditScore(int x)
    {
        currentScore += x;

        scoreText.text = "£" + currentScore.ToString();
        instructionHandler.ResetInstructions();

    }
}
